import 'package:flutter/material.dart';
import 'package:my_notes_sqflite/provider/todo_provider.dart';
import 'package:provider/provider.dart';

class EditTodoScreen extends StatefulWidget {
  const EditTodoScreen({
    Key? key,
    required this.id,
    required this.title,
    required this.description,
    required this.date}) : super(key: key);
  final String id;
  final String title;
  final String description;
  final String date;
  @override
  State<EditTodoScreen> createState() => _EditTodoScreenState();
}

class _EditTodoScreenState extends State<EditTodoScreen> {

  final TextEditingController _titleController=TextEditingController();
  final TextEditingController _descriptionController=TextEditingController();
  final TextEditingController _dateController=TextEditingController();

@override
  void initState() {
  _titleController.text=widget.title;
  _descriptionController.text=widget.description;
  _dateController.text=widget.date;
    super.initState();
  }

@override
  void dispose() {
  _titleController.dispose();
  _descriptionController.dispose();
  _dateController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {

  final todoProvider=Provider.of<TodoProvider>(context,listen: false);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Todo'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [

            Padding(
              padding: const EdgeInsets.all(15),
              child: TextField(
                controller: _titleController,
                decoration: const InputDecoration(
                    hintText:'title',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                ),
              ),
            ),


            Padding(
              padding: const EdgeInsets.all(15),
              child: TextField(
                controller: _descriptionController,
                decoration: const InputDecoration(
                    hintText:'Description',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                ),
              ),
            ),


            Padding(
              padding: const EdgeInsets.all(15),
              child: TextField(
                controller: _dateController,
                decoration: const InputDecoration(
                    hintText:'date',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                ),
              ),
            ),

           ElevatedButton(
             onPressed: ()async {
              await todoProvider.updateTitle(
                widget.id,
                _titleController.text,
              );
              await todoProvider.updateDescription(
                widget.id,
                _descriptionController.text,
            );
              await  todoProvider.updateDate(
                widget.id,
               _dateController.text,
            );
              Navigator.of(context).pop();

               },
               child: Text('Edit'),

           )


          ],
        ),
      ),
    );
  }
}
