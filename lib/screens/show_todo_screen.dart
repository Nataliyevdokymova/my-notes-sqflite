import 'package:flutter/material.dart';
import 'package:my_notes_sqflite/provider/todo_provider.dart';
import 'package:my_notes_sqflite/screens/add_todo_screen.dart';
import 'package:my_notes_sqflite/screens/edit_todo_screen.dart';
import 'package:provider/provider.dart';


class ShowTodoScreen extends StatelessWidget {
  const ShowTodoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width=MediaQuery.of(context).size.width;
    final height=MediaQuery.of(context).size.height;
    final todoP=Provider.of<TodoProvider>(context, listen: false);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
       child: const Icon(Icons.add),
        onPressed: (){
         Navigator.of(context).push(MaterialPageRoute(builder: (context)=>const AddTodoScreen()),
         );
        },
      ),
      appBar: AppBar(
        title: const Text('Todo'),
        actions: [
          IconButton(
            onPressed: (){
              showDialog(
                useSafeArea: true,
                context: context,
                builder: (context)=>AlertDialog(
                  scrollable: true,
                  title: const Text('Delete All'),
                  content: const Text('Do you want to delete all data?'),
                  actions: [
                    ElevatedButton(
                        onPressed: ()async {
                          await todoP.deleteTable('todo');
                          Navigator.pop(context);
                        },
                        child: const Text('Yes'),
                    ),

                    ElevatedButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      child: const Text('No'),
                    ),


                  ],
                ),
              );

            },
            icon: const Icon(
                Icons.delete_forever_outlined,
                color: Colors.white,
            ),
          ),
        ],
      ),
      body: FutureBuilder(
          future: Provider.of<TodoProvider>(context, listen: false).selectData(),
        builder: (context, snapshot){


            if (snapshot.connectionState==ConnectionState.done){
              return Consumer<TodoProvider>(
                builder:(context, todoProvider,child) {
                  return todoProvider.todoItem.isNotEmpty
                      ?ListView.builder(
                    itemCount: todoProvider.todoItem.length,
                    itemBuilder: (context, index){
                      return Dismissible(
                        key: ValueKey(todoProvider.todoItem[index].id),
                        background: Container(
                          margin: EdgeInsets.all(width*0.01),
                         padding: EdgeInsets.all(width*0.03),
                          decoration: BoxDecoration(
                              color: Colors.red,
                          borderRadius: BorderRadius.circular(width*0.03),
                          ),
                          alignment: Alignment.centerLeft,
                          height: height*0.02,
                          width: width,
                          child: const Icon(
                            Icons.delete_forever_outlined,
                            color: Colors.white,
                          ),
                        ),

                      secondaryBackground:Container(
                        padding: EdgeInsets.all(width*0.03),
                        margin: EdgeInsets.all(width*0.01),
                        width: width,
                        height: height*0.02,
                        decoration:BoxDecoration(
                          color: Colors.cyan,
                          borderRadius:
                             BorderRadius.circular(width*0.03),
                        ),
                        alignment: Alignment.centerRight,
                        child: const Icon(
                          Icons.edit,
                          color: Colors.white,
                        ),
                      ) ,


                        confirmDismiss: (DismissDirection direction)async{
                          if (direction==DismissDirection.endToStart){
                            return Navigator.of(context).push(
                              MaterialPageRoute(builder: (context)=>EditTodoScreen(
                                  id:todoProvider.todoItem[index].id,
                                  title:todoProvider.todoItem[index].title,
                                  description:todoProvider.todoItem[index].description,
                                  date:todoProvider.todoItem[index].date,
                              ),
                              ),
                            );
                          }

                        else{

              showDialog(
                useSafeArea: true,
              context: context,
                  builder: (context)=>AlertDialog(
                    scrollable: true,
                    title: const Text('Delete'),
                    content: const Text('Do you want to delete it?'),
                    actions: [



                      ElevatedButton(
                        onPressed: (){
                        todoProvider.deleteById(
                          todoProvider.todoItem[index].id
                        );
                            todoProvider.todoItem.remove(
                                todoProvider.todoItem[index]
                            );
                           Navigator.pop(context);
                           },
                          child: const Text('Yes'),
                      ),

                      ElevatedButton(onPressed: (){
                        Navigator.pop(context);
                      },
                        child: const Text('No'),
                      ),

                            ],
                           ),
                         );
                        }
                        },


                        child: Card(
                          elevation: 5,
                          child: ListTile(
                            style: ListTileStyle.drawer,
                            title: Text (
                                todoProvider.todoItem[index].title,
                            ),
                            subtitle: Text(
                                todoProvider.todoItem[index].description,
                            ),
                            trailing: Text(todoProvider.todoItem[index].date,
                            ),

                          ),
                        ),
                      );
                    },
                  )
                      : const Center(
                    child: Text(
                      'List is empty',
                      style:TextStyle(
                       color: Colors.black,
                        fontSize: 35,
                      ),
                    ),
                  );
                },
              );

            } else {

              return const Center(
              child: CircularProgressIndicator(),

              );

            }
        }
      ),
    );
  }
}
