import 'package:flutter/material.dart';
import 'package:my_notes_sqflite/provider/todo_provider.dart';
import 'package:provider/provider.dart';

class AddTodoScreen extends StatefulWidget {
  const AddTodoScreen({Key? key}) : super(key: key);

  @override
  State<AddTodoScreen> createState() => _AddTodoScreenState();
}

class _AddTodoScreenState extends State<AddTodoScreen> {

  TextEditingController _titleController=TextEditingController();
  TextEditingController _descriptionController=TextEditingController();
  TextEditingController _dateController=TextEditingController();

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    _dateController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    final todoProvider=Provider.of<TodoProvider>(context,listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Todo'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(15),
            child: TextField(
              controller: _titleController,
              decoration: const InputDecoration(hintText:'Title'),
              ),
          ),

          Padding(
            padding: const EdgeInsets.all(15),
            child: TextField(
              controller: _descriptionController,
              decoration: const InputDecoration(hintText:'Description'),
            ),
          ),


          Padding(
            padding: const EdgeInsets.all(15),
            child: TextField(
              controller: _dateController,
              decoration: const InputDecoration(hintText:'Date'),
            ),
          ),

ElevatedButton(
    onPressed: ()async{

      await todoProvider.insertData(
          _titleController.text,
          _descriptionController.text,
          _dateController.text,
      );
      _titleController.clear();
      _descriptionController.clear();
      _dateController.clear();
      Navigator.pop(context);
    },
    child: Text('Insert'),
       )


        ],

       ),
      );
  }
}
