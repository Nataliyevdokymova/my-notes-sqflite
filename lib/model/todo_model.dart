class TodoModel{

late final String id;
late String title;
late String description;
late String date;

TodoModel({
  required this.id,
  required this.title,
  required this.description,
  required this.date,
});
}